#include "ripv2.h"
#include "udp.h"
#include "ipv4.h"
#include "ipv4_route_table.h"
#include "ipv4_config.h"
#include "arp.h"
#include "eth.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libgen.h>
#include <arpa/inet.h>
#include <timerms.h>
#include <time.h>


int main ( int argc, char * argv[] ){

    // //Cargamos el fichero de configuracion y las tablas de rutas RIP
    if ( argc != 4 ) {
        printf("        <string>: Archivo de configuración del Servidor\n");
        printf("        <string>: Archivo de tabla de rutas IP del Servidor\n");
        printf("        <string>: Archivo de tabla de rutas RIP del Servidor\n");
        exit(-1);
    }

    char * config_file = argv[1];
    char * route_file_ip = argv[2];
    char * route_file_rip = argv[3];

    //abrimos la capa udp
    udp_layer_t * layer = udp_open(config_file, route_file_ip, 520);
    if (layer == NULL) {
        printf("ERROR en udp_open()\n");
        exit(-1);
    }

    //Creamos la tabla
    ripv2_table_t * table = ripv2_table_create();

    int n_table_entries = ripv2_table_read(route_file_rip, table);
    if(n_table_entries < 0){
        printf("Error al leer la tabla rip");
        exit(-1);
    }
    
    printf("\nTABLE:\n");
    ripv2_table_print(table);
    printf("\n\n");

    /// PREPARAMOS RIP REQUEST 
    ipv4_addr_t multicast_addr = { 224, 0, 0, 9 };
    int i, j, recv_bytes, routes_recv;
    long int timeout, time_left;
    uint16_t ripv2_port = 520;
    ripv2_entry_t * aux_entry;
    ipv4_addr_t sender_ip;

    ripv2_frame_t frame;
    ripv2_route_t request_route; //RFC RIP para hacer un request

    request_route.family = htons(0);
    request_route.tag = 0;
    request_route.metric = htonl(16);//métrica infinito
    memcpy(request_route.ipv4_address, IPv4_ZERO_ADDR, 4);
    memcpy(request_route.ipv4_mask, IPv4_ZERO_ADDR, 4);
    memcpy(request_route.next_hop, IPv4_ZERO_ADDR, 4);
    
    frame.type = 1; //1- Request
    frame.version = 2; //version 2
    frame.routing_domain = 0;
    frame.routes[0] = request_route;

    /// ENVIAMOS REQUEST POR MULTICAST
    int sent_bytes = udp_send(layer, multicast_addr, ripv2_port, (unsigned char *)&frame, 24);

    if (sent_bytes <= 0) {
        printf("ERROR en udp_send()\n");
        exit(-1);
    }

    // 1500-20-8
    unsigned char buffer[ETH_MTU - IPv4_HEADER_SIZE - UDP_HEADER_SIZE];
    ripv2_frame_t frame_recv;


    do{

/// RECIBIMOS RIP RESPONSE PARA RELLENAR NUESTRA TABLA
        recv_bytes = udp_recv(layer, sender_ip, &ripv2_port, buffer, sizeof(ripv2_frame_t), 2000);
        if (recv_bytes < 1) {
            printf("ERROR en udp_recv()\n");
            exit(-1);
        }
        memcpy(&frame_recv, &buffer, recv_bytes - IPv4_HEADER_SIZE - UDP_HEADER_SIZE);
    }while(frame_recv.type != 2);

    routes_recv = (recv_bytes - RIPv2_HEADER_SIZE - IPv4_HEADER_SIZE - UDP_HEADER_SIZE)/sizeof(ripv2_route_t);

    printf("We have received %d routes in the response.\n", routes_recv);

    if(frame_recv.type == 2){   //RESPONSE RECEIVED

        printf("\nROUTES RECEIVED:\n");
        for(i = 0; i<routes_recv; i++){
            ripv2_route_print(&frame_recv.routes[i]);


         //Función que recorre toda la tabla de rutas buscando rutas que contengan la dirección IPv4 indicada. Usando para ello'ipv4_route_lookup()'.
        //Se devuelve la ruta con el prefijo más específico (con la máscara de subred mayor)
        //'table': Tabla de rutas en la que buscar la dirección IPv4 destino.
        //'addr': Dirección IPv4 destino a buscar.

            aux_entry = ripv2_route_table_lookup(table, frame_recv.routes[i].ipv4_address);

            if(aux_entry != NULL){
                if(ntohl(frame_recv.routes[i].metric)+1 < aux_entry->route.metric){//recibimos mejor métrica
                    //printf("\n<\n");
                    //memcpy(&aux_entry->route, &frame_recv.routes[i], IPv4_ADDR_SIZE);
                    aux_entry->route.metric = ntohl(frame_recv.routes[i].metric)+1;
                    memcpy(aux_entry->route.next_hop, sender_ip, IPv4_ADDR_SIZE);
                    memcpy(aux_entry->route.ipv4_mask, frame_recv.routes[i].ipv4_mask, IPv4_ADDR_SIZE);
                    timerms_reset(&aux_entry->timer, 300000);

    
                }else if(ntohl(frame_recv.routes[i].metric)+1 == aux_entry->route.metric){//métrica igual. mismo router
                    //printf("\n==\n");
                    time_left = timerms_left(&aux_entry->timer);
                    if(time_left < 300000) //actualizamos el timer si era menos de 180s
                        timerms_reset(&aux_entry->timer, 300000);
                    memcpy(aux_entry->route.next_hop, sender_ip, IPv4_ADDR_SIZE);
                    memcpy(aux_entry->route.ipv4_mask, frame_recv.routes[i].ipv4_mask, IPv4_ADDR_SIZE);

                }else if(ntohl(frame_recv.routes[i].metric)+1 > aux_entry->route.metric){//métrica mayor
                    //printf("\n>\n");
                    if(memcmp(aux_entry->route.next_hop, sender_ip, IPv4_ADDR_SIZE)==0){
                        //memcpy(&aux_entry->route, &frame_recv.routes[i], IPv4_ADDR_SIZE);
                        aux_entry->route.metric = ntohl(frame_recv.routes[i].metric)+1;
                        timerms_reset(&aux_entry->timer, 300000);
                    }
                }
            }else{ //recibimos una ruta que no teníamos en nuestra tabla
                aux_entry = ripv2_entry_create(frame_recv.routes[i].ipv4_address, 
                                                frame_recv.routes[i].ipv4_mask, 
                                                sender_ip, 
                                                ntohl(frame_recv.routes[i].metric)+1, 
                                                300000);
                ripv2_table_add(table, aux_entry);
                n_table_entries++;       
            }
        } 
    }

    printf("\nTABLE:\n");
    ripv2_table_print(table);
    printf("\n\n");

    while(1){
        timeout = 300000;

        //BUCLE PARA ENCONTRAR EL TIMEOUT MÁS BAJO Y USARLO EN EL RECV
        for(i = 0; i<RIPv2_TABLE_SIZE; i++){
            if(table->entries[i] != NULL){

                time_left = timerms_left(&table->entries[i]->timer);

                if(time_left > 120000) //para diferenciar entre entradas a infinito y entradas normales
                    time_left -= 120000;

                if(time_left<timeout)
                        timeout = time_left;
            }

        }

        printf("timeout: %lds\n", timeout/1000);

        //unsigned char buffer[ETH_MTU - IPv4_HEADER_SIZE - UDP_HEADER_SIZE];
        ripv2_frame_t frame_recv;

        recv_bytes = udp_recv(layer, sender_ip, &ripv2_port, buffer, sizeof(ripv2_frame_t), timeout);
        

        //BUCLE PARA ELIMINAR RUTAS QUE HAYAN EXPIRADO
        for(i = 0; i<RIPv2_TABLE_SIZE; i++){ 
            if(table->entries[i] != NULL){
                time_left = timerms_left(&table->entries[i]->timer);
                if(time_left<120000){

                    if(time_left>0){
                        table->entries[i]->route.metric = 16;
                    }else{
                        aux_entry = table->entries[i];
                        table->entries[i] = NULL;
                        ripv2_entry_free(aux_entry);
                        n_table_entries--;
                    }

                }
/*
                if(time_left <= 0){ //&& table->entries[i]->route.metric < 16 (garbage collection timer)
                    aux_entry = table->entries[i];
                    table->entries[i] = NULL;
                    ripv2_entry_free(aux_entry);
                    n_table_entries--;
                }
*/
            }
        }

        if (recv_bytes < 1) {
            printf("\nTABLE:\n");
            ripv2_table_print(table);
            printf("\n\n");
            printf("Number of entries in the table: %d\n", n_table_entries);
            continue;//volvemos a empezar el bucle while(1)
        }


        memcpy(&frame_recv, &buffer, recv_bytes - IPv4_HEADER_SIZE - UDP_HEADER_SIZE);

        routes_recv = (recv_bytes - RIPv2_HEADER_SIZE - IPv4_HEADER_SIZE - UDP_HEADER_SIZE)/sizeof(ripv2_route_t);

        printf("We have received %d routes in the response.\n", routes_recv);
/*
        for(int i = 0; i<routes_recv; i++){

            ripv2_route_print(&frame_recv.routes[i]);

        }
*/

        if(frame_recv.type == 1){   //REQUEST RECEIVED -> ENVIAMOS RESPONSE CON TODA NUESTRA TABLA

            frame.type = 2;
            frame.version = 2;
            frame.routing_domain = htons(0);
            j = 0;
            for(i = 0; i < RIPv2_TABLE_SIZE; i++){
                if(table->entries[i] != NULL){
                    frame.routes[j].family = htons(table->entries[i]->route.family);
                    frame.routes[j].tag = htons(table->entries[i]->route.tag);
                    memcpy(frame.routes[j].ipv4_address, table->entries[i]->route.ipv4_address, IPv4_ADDR_SIZE);
                    memcpy(frame.routes[j].ipv4_mask, table->entries[i]->route.ipv4_mask, IPv4_ADDR_SIZE);
                    memcpy(frame.routes[j].next_hop, table->entries[i]->route.next_hop, IPv4_ADDR_SIZE);
                    frame.routes[j].metric = htonl(table->entries[i]->route.metric);
                    j++;
                }
            }
            sent_bytes = udp_send(layer, sender_ip, ripv2_port, (unsigned char *)&frame, 4+(20*n_table_entries)); //4 de la cabecera y 20 por entrada

        }

        else if(frame_recv.type == 2){   //RESPONSE RECEIVED -> PROCESAMOS ENTRADAS RECIBIDAS

            printf("\nROUTES RECEIVED:\n");
            for(i = 0; i<routes_recv; i++){
                ripv2_route_print(&frame_recv.routes[i]);

                aux_entry = ripv2_route_table_lookup(table, frame_recv.routes[i].ipv4_address);

                if(aux_entry != NULL){
                    if(ntohl(frame_recv.routes[i].metric)+1 < aux_entry->route.metric){//recibimos mejor métrica
                        //printf("\n<\n");
                        //memcpy(&aux_entry->route, &frame_recv.routes[i], IPv4_ADDR_SIZE);
                        aux_entry->route.metric = ntohl(frame_recv.routes[i].metric)+1;
                        memcpy(aux_entry->route.next_hop, sender_ip, IPv4_ADDR_SIZE);
                        memcpy(aux_entry->route.ipv4_mask, frame_recv.routes[i].ipv4_mask, IPv4_ADDR_SIZE);
                        timerms_reset(&aux_entry->timer, 300000);
                    }else if(ntohl(frame_recv.routes[i].metric)+1 == aux_entry->route.metric){//métrica igual
                        //printf("\n==\n");
                        time_left = timerms_left(&aux_entry->timer);
                        if(time_left < 300000) //actualizamos el timer si era menos de 180s
                            timerms_reset(&aux_entry->timer, 300000);
                        memcpy(aux_entry->route.next_hop, sender_ip, IPv4_ADDR_SIZE);//actualizamos next hop (lo normal es que sea el mismo)
                        memcpy(aux_entry->route.ipv4_mask, frame_recv.routes[i].ipv4_mask, IPv4_ADDR_SIZE);
                    }else if(ntohl(frame_recv.routes[i].metric)+1 > aux_entry->route.metric){//métrica mayor
                        //printf("\n>\n");
                        if(memcmp(aux_entry->route.next_hop, sender_ip, IPv4_ADDR_SIZE)==0){
                            //memcpy(&aux_entry->route, &frame_recv.routes[i], IPv4_ADDR_SIZE);
                            aux_entry->route.metric = ntohl(frame_recv.routes[i].metric)+1;
                            timerms_reset(&aux_entry->timer, 300000);
                        }
                    }
                }else{
                    aux_entry = ripv2_entry_create(frame_recv.routes[i].ipv4_address, 
                                                    frame_recv.routes[i].ipv4_mask, 
                                                    sender_ip, 
                                                    ntohl(frame_recv.routes[i].metric)+1, 
                                                    300000);
                    ripv2_table_add(table, aux_entry);
                    n_table_entries++;       
                }
            } 
        }

        printf("\nTABLE:\n");
        ripv2_table_print(table);
        printf("\n\n");
        printf("Number of entries in the table: %d\n", n_table_entries);

    }

    ripv2_table_free(table);

}
